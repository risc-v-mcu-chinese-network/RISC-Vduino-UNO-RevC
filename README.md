# RISC-Vduino UNO RC
![输入图片说明](.gitee/RISC-VduinoUNO%20RC.jpg)
#### 介绍
RISC-Vduino UNO RC是由[RISC-V单片机中文网](http://www.risc-v1.com/RISCV.htm)（[RISC-V单片机中文社区]www.RISC-Vduino.cc）贡献者EngineerXu设计的一块开源硬件板，它是采用的是全球流行的RISC-V开源指令集设计的MCU，他可以使用自由和开源软件编写控制代码，RISC-Vduino UNO和Arduino UNO一样是面向创客的开源硬件板卡，可以像Arduino一样进行有趣的电子DIY制作！
RISC-Vduino使用32位开源指令集MCU（eg,RISC-V），Arduino使用8位AVR不开源指令集MCU（eg.AVR，ARM）；不幸的是[Arduino UNO采用的MCU厂商已经宣布涨价或者停产](https://www.risc-v1.com/forum.php?mod=viewthread&tid=200&highlight=microchip/)
RISC-Vduino UNO板子的32bits MCU频率可以达到72 ~ 108MHz，Arduino UNO板子的8bits MCU频率可以达到16 ~ 20MHz；
RISC-Vduino UNO板子的微控制器包含Analog-GPIO/Digital-GPIO/PWM/DMA/ADC/DAC/I2C/I2S/SPI/USART/RS485/CAN可以用来处理更多的数学运算与通信控制，Arduino UNO板子的微控制器通常需要扩展板才能运行RS485/CAN等功能.带来了经济成本的增加；
RISC-Vduino UNO板子更适合AIoT时代电子DIY制作，经济型和功能性更胜一筹，而Arduino UNO适合过去传统时代电子DIY制作。

#### 软件架构
RISC-Vduino UNO RevC采用WCH的CH32V103C8T6 MCU，运行频率72MHz，软件支持寄存器操作或者库函数操作，同时也支持RTOS（eg.FreeRTOS，Zephry，RT-Thread，Harmony LiteOS-M，μC/OS-II......）
可以适配曾经Arduino扩展的传感器，执行器等硬件生态

#### 安装教程

1.  安装电脑系统(Windows/Linux/Mac OS X/[UOS](https://home.uniontech.com/))
2.  安装RISC-Vduino UNO RevC板子开发环境[MounRiver Stuido IDE](http://file.mounriver.com/upgrade/MounRiver_Studio_Setup_V170.zip)（也可以安装其他开发环境https://www.risc-v1.com/forum.php?gid=68）
3.  安装一个[Arduino IDE](http://[输入链接说明](https://www.arduino.cc/en/software))串口助手用于串口监视RISC-Vduino UNO板子的运行情况和交互信息(也可以安装其他串口助手如RISC-Vduino串口助手，[串口猎人](https://gitee.com/risc-v-mcu-chinese-network/riscv-board-application-development-tools-kit/raw/master/Communication_SerialPort/%E4%B8%B2%E5%8F%A3%E7%8C%8E%E4%BA%BA%20(%20Serial%20Hunter%20)%20V31%20setup.zip)，[sscom32](https://gitee.com/risc-v-mcu-chinese-network/riscv-board-application-development-tools-kit/tree/master/Communication_SerialPort)......)

#### 使用说明
![输入图片说明](01_2%20(1).jpg)
1.  准备一台带有操作系统OS的 笔记本电脑PC 或者台式电脑PC 或者单板计算机SBC，保证其正常工作
2.  在电脑安装[MounRiver Stuido IDE](http://file.mounriver.com/upgrade/MounRiver_Studio_Setup_V170.zip)最新版[http://www.mounriver.com/download] 或者Eclipse IDE [https://www.eclipse.org/downloads]
3.  在电脑安装一个串口助手工具如RISC-Vduino串口助手或者串口猎人或者Arduino IDE[串口助手](https://gitee.com/risc-v-mcu-chinese-network/riscv-board-application-development-tools-kit/raw/master/Communication_SerialPort/%E4%B8%B2%E5%8F%A3%E7%8C%8E%E4%BA%BA%20(%20Serial%20Hunter%20)%20V31%20setup.zip)
4.  准备好[RISC-Vduino UNO板子](https://shop323709919.taobao.com/index.htm?spm=a1z10.5-c.w5002-23639506074.2.180036769JB2O8)
5.  准备好MiniUSB线，MicroUSB线，USB下载线，DC电源线
6.  将RISC-Vduino UNO板子通过USB方口下载线接入电脑USB口，MicroUSB线也接入电脑USB口，打开串口助手工具，即可看到来自RISC-Vduino UNO板子对电脑的问候一句句对话！
![输入图片说明](01_2.jpg)
7.  参考下载学习RISC-V MCU基础程序设计与开发入门
    资料a. [移植好的教程代码](https://gitee.com/risc-v-mcu-chinese-network/RISC-Vduino-UNO-RevC/tree/develop/SoftwareFirmware_BASIC)，
    资料b. [RISC-Vduino UNO RC开发板项目大攻略](https://www.risc-v1.com/thread-2977-1-1.html)
    资料c. [手把手开发系列教程CH32V103](https://www.risc-v1.com/thread-1629-1-1.html)
8.  参考学习RISC-Vduino UNO 过程有问题请在(https://www.RISC-V1.com) RISC-V单片机中文网对应RISC-V MCU芯片版块留言，或者QQ群728522642/微信群/邮件support@RISC-V1.com联系众多志愿者工程师。
#### 参与贡献

1.  Fork 本仓库
2.  新建 RISC-Vduino UNO RC 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
