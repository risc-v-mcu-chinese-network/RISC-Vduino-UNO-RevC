# RISC-Vduino UNO RC

#### Description
RISC-Vduino UNO RC是基于RISC-V开源指令集设计的开源硬件板卡，可以使用自由和开源软件编写控制代码，RISC-Vduino UNO和Arduino UNO一样是面向创客的开源硬件板卡，可以像Arduino一样进行有趣的电子DIY制作！
RISC-Vduino使用32位开源指令集MCU（eg,RISC-V），Arduino使用8位AVR不开源指令集MCU（eg.AVR，ARM）

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
